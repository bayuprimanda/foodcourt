<form id="form-meja" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		
		<input type="text" id="no_meja" name="no_meja" class="form-control" value="${item.no_meja}">
		<input type="text" id="tipe_meja" name="tipe_meja" class="form-control" value="${item.tipe_meja}">
		<input type="text" id="sts_meja" name="sts_meja" class="form-control" value="${item.sts_meja}">
		<input type="text" id="ket_meja" name="ket_meja" class="form-control" value="${item.ket_meja}">
		
		<div class="form-group">
			<p>Apakah anda yakin mau menghapus data meja ${item.no_meja} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
