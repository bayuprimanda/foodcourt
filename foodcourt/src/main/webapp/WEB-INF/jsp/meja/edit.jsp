<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<form id="form-meja" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		
		<div class="form-group">
			<label class="control-label col-md-2">No.Meja</label>
			<div class="col-md-6">
				<input type="text" id="no_meja" name="no_meja" class="form-control" value="no_meja">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe Meja</label>
			<div class="col-md-6">
				<select name="tipe_meja" id="tipe_meja">
					<option value="Family"
						<c:if test="${tipe_meja == 'Family'}">
							<c:out value="selected"/>
						</c:if>>Family
					</option>
					<option value="Smoking-Date"
						<c:if test="${tipe_meja == 'Smoking-Date'}">
							<c:out value="selected"/>
						</c:if>>Smoking-Date
					</option>
					<option value="Non-Smoking-Date"
						<c:if test="${tipe_meja == 'Non-Smoking-Date'}">
							<c:out value="selected"/>
						</c:if>>Non-Smoking-Date
					</option>
					<option value="Non-smoking-Family"
						<c:if test="${tipe_meja == 'Non-smoking-Family'}">
							<c:out value="selected"/>
						</c:if>>Non-smoking-Family
					</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Status Meja</label>
			<div class="col-md-6">
				<input type="radio" name="sts_meja" value="available"
					<c:if test="${sts_meja == 'available'}">
						<c:out value="checked"/>
					</c:if>>available
				<br>
				<input type="radio" name="sts_meja" value="sold_out"
					<c:if test="${sts_meja == 'sold_out'}">
						<c:out value="checked"/>
					</c:if>>sold_out
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan Meja</label>
			<div class="col-md-6">
				<input type="text" id="ket_meja" name="ket_meja" class="form-control">
			</div>					
		</div>
		
	</div>
	
	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>