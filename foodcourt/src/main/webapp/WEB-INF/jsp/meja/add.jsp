<form id="form-meja" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		<div class="form-group">
			<label class="control-label col-md-2">No.Meja</label>
			<div class="col-md-6">
				<input type="text" id="no_meja" name="no_meja" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Tipe Meja</label>
			<div class="col-md-6">
				<select name="tipe_meja" id="tipe_meja">
					<option>Pilih</option>
					<option value="Family">Family</option>
					<option value="Smoking-Date">Smoking-Date</option>
					<option value="Non-Smoking-Date">Non-Smoking-Date</option>
					<option value="Non-smoking-Family">Non-smoking-Family</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Status Meja</label>
			<div class="col-md-6">

				<input type="radio" name="sts_meja" value="available">Available <br>
				<input type="radio" name="sts_meja" value="sold_out">Sold Out

			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Keterangan Meja</label>
			<div class="col-md-6">
				<input type="text" id="ket_meja" name="ket_meja" class="form-control">
			</div>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>