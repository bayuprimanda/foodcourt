<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:forEach var="item" items="${list}">
	<tr>
		<td>${item.no_meja}</td>
		<td>${item.tipe_meja}</td>
		<td>${item.sts_meja}</td>
		<td>${item.ket_meja}</td>
		<td>
			<button type="button" id="btn-edit" class="btn btn-success btn-xs btn-edit" value="${item.id_meja}"><i class="fa fa-edit"></i></button>
			<button type="button" id="btn-delet" class="btn btn-danger btn-xs btn-delete" value="${item.id_meja}"><i class="fa fa-trash"></i></button>
		</td>
	</tr>
</c:forEach>