<form id="form-bahan" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		
		<input type="text" id="nama" name="nama" class="form-control" value="${item.nama }">
		<input type="text" id="jenis" name="jenis" class="form-control" value="${item.jenis }">
		<input type="text" id="stok" name="stok" class="form-control" value="${item.stok }">
		<input type="text" id="satuan" name="satuan" class="form-control" value="${item.satuan }">
		<input type="text" id="harga" name="harga" class="form-control" value="${item.harga }">
		
		<div class="form-group">
			<p>Apakah anda yakin mau menghapus data Bahan ${item.nama} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
