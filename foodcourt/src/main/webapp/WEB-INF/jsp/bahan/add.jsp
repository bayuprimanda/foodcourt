<form id="form-bahan" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jenis Bahan</label>
			<div class="col-md-6">
				<select id="jenis" name="jenis">
					<option>Pilih</option>
					<option value="Daging">Daging</option>
					<option value="Sayuran">Sayuran</option>
					<option value="Buah">Buah</option>
					<option value="Ikan">Ikan</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-6">
				<input type="text" id="stok" name="stok" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Satuan Bahan</label>
			<div class="col-md-6">
				<select id="satuan" name="satuan">
					<option>Pilih</option>
					<option value="Kg">Kg</option>
					<option value="Liter">Liter</option>
					<option value="Lusin">Lusin</option>
					<option value="Pcs">PCS</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Harga</label>
			<div class="col-md-6">
				<input type="text" id="harga" name="harga" class="form-control">
			</div>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>