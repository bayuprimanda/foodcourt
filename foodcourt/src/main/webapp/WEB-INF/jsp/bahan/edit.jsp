<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<form id="form-bahan" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="update">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="nama" name="nama" class="form-control" value="${item.nama}">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jenis Bahan</label>
			<div class="col-md-6">
				<select id="jenis" name="jenis">
					<option value="tipe1"
						<c:if test="${item.jenis == 'tipe1'}">
							<c:out value="selected"/>
						</c:if>>Daging
					</option>
					
					<option value="tipe2"
						<c:if test="${item.jenis == 'tipe2'}">
							<c:out value="selected"/>
						</c:if>>Sayuran
					</option>
					
					<option value="tipe3"
						<c:if test="${item.jenis == 'tipe3'}">
							<c:out value="selected"/>
						</c:if>>Buah
					</option>
					
					<option value="tipe4"
						<c:if test="${item.jenis == 'tipe4'}">
							<c:out value="selected"/>
						</c:if>>Ikan
					</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Stok</label>
			<div class="col-md-6">
				<input type="text" id="stok" name="stok" class="form-control" value="${item.stok}">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Satuan Bahan</label>
			<div class="col-md-6">
				<select id="satuan" name="satuan">
					<option value="satuan1"
						<c:if test="${item.satuan == 'satuan1'}">
							<c:out value="selected"/>
						</c:if>>Kg
					</option>
					
					<option value="satuan2"
						<c:if test="${item.satuan == 'satuan2'}">
							<c:out value="selected"/>
						</c:if>>Liter
					</option>
					
					<option value="satuan3"
						<c:if test="${item.satuan == 'satuan3'}">
							<c:out value="selected"/>
						</c:if>>Lusin
					</option>
					
					<option value="satuan4"
						<c:if test="${item.satuan == 'satuan4'}">
							<c:out value="selected"/>
						</c:if>>PCS
					</option>
				</select>
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Harga</label>
			<div class="col-md-6">
				<input type="text" id="harga" name="harga" class="form-control" value="${item.harga}">
			</div>					
		</div>
		
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
