<form id="form-menu" action="update" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control"
			value="update">

		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="n_mkn" name="n_mkn" class="form-control"
					value="${item.n_mkn}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-2">Jenis</label>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-2">Jenis</label>
					<div class="col-md-6">
						<select name="jns_mkn">
							<option>Makanan</option>
							<option>Minuman</option>
							<option>Snack</option>
							<option>Buah</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-2">Harga</label>
		<div class="col-md-6">
			<input type="text" id="hrg_mkn" name="hrg_mkn" class="form-control"
				value="${item.hrg_mkn}">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-md-2">Stock</label>
		<div class="col-md-6">
			<input type="radio" id="stock1" name="stock" class="form-control"
				value="ada">Available <input type="radio" id="stock2"
				name="stock" class="form-control" value="habis">Sold Out
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>