<form id="form-menu" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="n_mkn" name="n_mkn" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Jenis</label>
			<div  class="col-md-6">
				<select name="jns_mkn">
				<option>Makanan</option>
				<option>Minuman</option>
				<option>Snack</option>
				<option>Buah</option>
				</select>
			</div>					
		</div>
			
	<div class="form-group">
			<label class="control-label col-md-2">Harga</label>
			<div class="col-md-6">
				<input type="text" id="hrg_mkn" name="hrg_mkn" class="form-control">
			</div>					
		</div>
		
	<div class="form-group">
			<label class="control-label col-md-2">Stock</label>
			<div  class="col-md-6">
				<input type="radio" name="stock" value="available">Available <br>
				<input type="radio" name="stock" value="sold_out">Sold Out
			</div>					
		</div>	
		</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>