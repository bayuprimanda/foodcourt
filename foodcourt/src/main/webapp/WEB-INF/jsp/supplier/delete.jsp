<form id="form-supplier" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="delete">
		<input type="text" id="id_sup" name="id_sup" class="form-control" value="${item.id_sup}">
		<input type="text" id="n_sup" name="n_sup" class="form-control" value="${item.n_sup}">
		<input type="text" id="tlp_sup" name="tlp_sup" class="form-control" value="${item.tlp_sup}">
		
		<div class="form-group">
			<p>Apakah anda yakin mau menghapus data menu ${item.n_sup} ?</p>				
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>
