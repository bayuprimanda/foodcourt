<form id="form-supplier" action="save" method="post">
	<div class="form-horizontal">
		<input type="hidden" id="proses" name="proses" class="form-control" value="insert">
		
		<div class="form-group">
			<label class="control-label col-md-2">ID</label>
			<div class="col-md-6">
				<input type="text" id="id_sup" name="id_sup" class="form-control">
			</div>					
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-2">Nama</label>
			<div class="col-md-6">
				<input type="text" id="n_sup" name="n_sup" class="form-control">
			</div>					
		</div>
			
	<div class="form-group">
			<label class="control-label col-md-2">Telepon</label>
			<div class="col-md-6">
				<input type="text" id="tlp_sup" name="tlp_sup" class="form-control">
			</div>					
		</div>
	</div>

	<div class="modal-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>