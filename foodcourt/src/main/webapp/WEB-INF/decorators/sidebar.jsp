<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="treeview">
				<a href="#"> 
					<i	class="fa fa-dashboard"></i> <span>Master Data</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/meja.html" class="menu-item"><i class="fa fa-circle-o"></i> Meja</a></li>
					<li><a href="${contextName}/menu.html" class="menu-item"><i class="fa fa-circle-o"></i> Menu</a></li>
					<li><a href="${contextName}/supplier.html" class="menu-item"><i class="fa fa-circle-o"></i> Supplier</a></li>
					<li><a href="${contextName}/bahan_mkn.html" class="menu-item"><i class="fa fa-circle-o"></i> Bahan Makanan</a></li>
					
				</ul>
			</li>
			<li>
				<a href="#"> 
					<i	class="fa fa-laptop"></i> <span>Purchase</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/purchase/request.html"><i class="fa fa-circle-o"></i> Request</a></li>
					<li><a href="${contextName}/purchase/order.html"><i class="fa fa-circle-o"></i> Order</a></li>
				</ul>
			</li>
			
			<li>
				<a href="#"> 
					<i	class="fa fa-pie-chart"></i> <span>Transaksi</span> 
					<span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i>	</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/transaksi/adjustment.html"><i class="fa fa-circle-o"></i> Request</a></li>
					<li><a href="${contextName}/transaksi/transfer.html"><i class="fa fa-circle-o"></i> Order</a></li>
				</ul>
			</li>
			
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>