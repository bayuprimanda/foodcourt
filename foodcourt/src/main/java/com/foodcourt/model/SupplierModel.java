package com.foodcourt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SUPPLIER")
public class SupplierModel {
	
	@Id
	private Integer id_sup;
	private String n_sup;
	private String tlp_sup;
	
	@Column(name="ID_SUP")
	@GeneratedValue
	public Integer getId_sup() {
		return id_sup;
	}
	public void setId_sup(Integer id_sup) {
		this.id_sup = id_sup;
	}
	
	@Column(name="NAMA_SUP")
	public String getN_sup() {
		return n_sup;
	}
	public void setN_sup(String n_sup) {
		this.n_sup = n_sup;
	}
	
	@Column(name="TELP_SUP")
	public String getTlp_sup() {
		return tlp_sup;
	}
	public void setTlp_sup(String tlp_sup) {
		this.tlp_sup = tlp_sup;
	}
	
}
