package com.foodcourt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MEJA")
public class MejaModel {
	
	private Integer id_meja;
	private Integer no_meja;
	private String tipe_meja;
	private String sts_meja;
	private String ket_meja;
	
	@Id
	@GeneratedValue
	@Column(name="ID_MEJA")
	public Integer getId_meja() {
		return id_meja;
	}
	public void setId_meja(Integer id_meja) {
		this.id_meja = id_meja;
	}
	
	@Column(name="NO_MEJA")
	public Integer getNo_meja() {
		return no_meja;
	}
	public void setNo_meja(Integer no_meja) {
		this.no_meja = no_meja;
	}
	
	@Column(name="TIPE_MEJA")
	public String getTipe_meja() {
		return tipe_meja;
	}
	public void setTipe_meja(String tipe_meja) {
		this.tipe_meja = tipe_meja;
	}
	
	@Column(name="STS_MEJA")
	public String getSts_meja() {
		return sts_meja;
	}
	public void setSts_meja(String sts_meja) {
		this.sts_meja = sts_meja;
	}
	
	@Column(name="KET_MEJA")
	public String getKet_meja() {
		return ket_meja;
	}
	public void setKet_meja(String ket_meja) {
		this.ket_meja = ket_meja;
	}
	
}
