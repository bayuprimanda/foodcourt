package com.foodcourt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foodcourt.model.SupplierModel;
import com.foodcourt.service.SupplierService;
import com.foodcourt.dao.SupplierDao;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService{
	
	@Autowired
	private SupplierDao dao;

	@Override
	public List<SupplierModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public SupplierModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.getById(id);
	}

	@Override
	public void update(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(SupplierModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}
