package com.foodcourt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foodcourt.model.MenuModel;
import com.foodcourt.service.MenuService;
import com.foodcourt.dao.MenuDao;

@Service
@Transactional
public class MenuServiceImpl implements MenuService{
	
	@Autowired
	private MenuDao dao;

	@Override
	public List<MenuModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public void insert(MenuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public MenuModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.getById(id);
	}

	@Override
	public void update(MenuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(MenuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}

}
