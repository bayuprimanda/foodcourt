package com.foodcourt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foodcourt.dao.MejaDao;
import com.foodcourt.model.MejaModel;
import com.foodcourt.service.MejaService;

@Service
@Transactional
public class MejaServiceImpl implements MejaService {

	@Autowired
	private MejaDao dao;
	
	@Override
	public List<MejaModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public MejaModel getById(int id_meja) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id_meja);
	}

	@Override
	public void insert(MejaModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
	}

	@Override
	public void update(MejaModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(MejaModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}
	
	

}
