package com.foodcourt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foodcourt.dao.BhnBakuDao;
import com.foodcourt.model.BhnBakuModel;
import com.foodcourt.service.BhnBakuService;

@Service
@Transactional
public class BhnBakuServiceImpl implements BhnBakuService{

	@Autowired
	private BhnBakuDao dao;
	
	@Override
	public List<BhnBakuModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.dao.get();
	}

	@Override
	public BhnBakuModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.dao.getById(id);
	}

	@Override
	public void insert(BhnBakuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.insert(model);
		
	}

	@Override
	public void update(BhnBakuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.update(model);
	}

	@Override
	public void delete(BhnBakuModel model) throws Exception {
		// TODO Auto-generated method stub
		this.dao.delete(model);
	}
	
	

}
