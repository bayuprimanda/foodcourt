package com.foodcourt.service;

import java.util.List;

import com.foodcourt.model.MenuModel;

public interface MenuService {
	
	public List<MenuModel> get() throws Exception;

	public void insert(MenuModel model) throws Exception;

	public MenuModel getById(int id) throws Exception;

	public void update(MenuModel model) throws Exception;

	public void delete(MenuModel model) throws Exception;
	
}
