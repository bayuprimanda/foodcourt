package com.foodcourt.service;

import java.util.List;

import com.foodcourt.model.MejaModel;

public interface MejaService {
	
	public List<MejaModel> get() throws Exception;

	public MejaModel getById(int id_meja) throws Exception;
	
	public void insert(MejaModel model) throws Exception;

	public void update(MejaModel model) throws Exception;

	public void delete(MejaModel model) throws Exception;
}
