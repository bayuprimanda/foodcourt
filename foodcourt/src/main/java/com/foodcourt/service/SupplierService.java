package com.foodcourt.service;

import java.util.List;

import com.foodcourt.model.SupplierModel;

public interface SupplierService {
	
	public List<SupplierModel> get() throws Exception;

	public void insert(SupplierModel model) throws Exception;

	public SupplierModel getById(int id) throws Exception;

	public void update(SupplierModel model) throws Exception;

	public void delete(SupplierModel model) throws Exception;
	
	
}
