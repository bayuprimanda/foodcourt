package com.foodcourt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.foodcourt.model.BhnBakuModel;
import com.foodcourt.service.BhnBakuService;

@Controller
public class BhnBakuController {
	
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private BhnBakuService service;
	
	@RequestMapping(value="/bahan")
	public String index(Model model){
		
		return "bahan";
	}
	
	@RequestMapping(value="/bahan/list")
	public String list(Model model){
		// membuat object list dari class Fakultas model
		List<BhnBakuModel> items = null;
		
		try {
			// object items diisi data dari method get
			items = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		// datanya kita kirim ke view, 
		// kita buat variable list kemudian diisi dengan object items
		model.addAttribute("list", items);
		
		return "bahan/list";
	}
	
	@RequestMapping(value="/bahan/add")
	public String add(){
		return "bahan/add";
	}
	
	// method untuk save data dari pop up (edit, insert, dan delet)
	@RequestMapping(value="/bahan/save")
	public String save(Model model, @ModelAttribute BhnBakuModel item, HttpServletRequest request){
		String proses = request.getParameter("proses");
		String result ="";
		
		try {
			if(proses.equals("insert")){
				this.service.insert(item);
			}else if(proses.equals("update")){
				this.service.update(item);
			}else if(proses.equals("delete")){
				this.service.delete(item);
			}
			
			result="berhasil";
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result ="gagal";
		}
		model.addAttribute("result",result);
		
		return "bahan/save";
	}
	
	// method untuk memunculkan pop up delete
	@RequestMapping(value="/bahan/edit")
	public String edit(Model model, HttpServletRequest request){
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		BhnBakuModel item = null;
		
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("item", item);
		return "bahan/edit";
	}
	
	// method untuk memunculkan pop up delete
	@RequestMapping(value="/bahan/delete")
	public String delete(Model model, HttpServletRequest request){
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		BhnBakuModel item = null;
		
		try {
			item = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		model.addAttribute("item", item);
		return "bahan/edit";
	}
	

}
