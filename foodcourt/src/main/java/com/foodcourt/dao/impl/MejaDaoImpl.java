package com.foodcourt.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.foodcourt.dao.MejaDao;
import com.foodcourt.model.MejaModel;

@Repository
public class MejaDaoImpl implements MejaDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<MejaModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<MejaModel> result = session.createQuery("from MejaModel").list();
		
		return result;
	}

	@Override
	public MejaModel getById(int id_meja) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		MejaModel result =  session.get(MejaModel.class, id_meja);
		
		return result;
	}

	@Override
	public void insert(MejaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		
		session.save(model);
	}

	@Override
	public void update(MejaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		
		session.update(model);
	}

	@Override
	public void delete(MejaModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		
		session.delete(model);
	}

	

}
