package com.foodcourt.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.foodcourt.dao.BhnBakuDao;
import com.foodcourt.model.BhnBakuModel;

@Repository
public class BhnBakuDaoImpl implements BhnBakuDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<BhnBakuModel> get() throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		List<BhnBakuModel> result = session.createQuery("from BhnBakuModel").list();
		return result;
	}

	@Override
	public BhnBakuModel getById(int id) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		BhnBakuModel result = session.get(BhnBakuModel.class, id);
		return result;
	}

	@Override
	public void insert(BhnBakuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(model);
		
	}

	@Override
	public void update(BhnBakuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(model);
		
	}

	@Override
	public void delete(BhnBakuModel model) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(model);
		
	}
	
	

}
