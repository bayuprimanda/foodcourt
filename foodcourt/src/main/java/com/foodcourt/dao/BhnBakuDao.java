package com.foodcourt.dao;

import java.util.List;

import com.foodcourt.model.BhnBakuModel;

public interface BhnBakuDao {
	
	public List<BhnBakuModel> get() throws Exception;

	public BhnBakuModel getById(int id) throws Exception;
	
	public void insert(BhnBakuModel model) throws Exception;

	public void update(BhnBakuModel model) throws Exception;

	public void delete(BhnBakuModel model) throws Exception;

}
